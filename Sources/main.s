;CarsgameGB
;Game Boy DMG WLA-DX
;Diego Haldi
;08.08.2018


.ROMDMG                         ;pour game boy originale
.NAME "CarsgameGB"
.CARTRIDGETYPE 0                ;type de cartouche
.RAMSIZE 0                      ;taille de la RAM de la cartouche (pas besoin pour ce jeu car pas de sauvgarde)
.COMPUTEGBCHECKSUM
.COMPUTEGBCOMPLEMENTCHECK
.LICENSEECODENEW "00"
.EMPTYFILL $00                  ;rempli le vide avec des 0

.MEMORYMAP
SLOTSIZE $4000
DEFAULTSLOT 0
SLOT 0 $0000
SLOT 1 $4000
.ENDME

.ROMBANKSIZE $4000              ;Deux banks de 16Ko
.ROMBANKS 2

.BANK 0 SLOT 0                   ;on se place sur la bank 0 du slot 0

.ENUM $C000
 p1posx db
 p2posx db
 flag db
 flag2 db
.ENDE

.ORG $0040
call      vblank                ;L'interruption VBlank tombe ici
reti

.ORG $0100                      ;� cette �dresse que PC est initialis� lorsque la Game Boy � termin� de faire son travail de v�rification de la cartouche
nop
jp    start

.ORG $0104
;Logo Nintendo, obligatoire
.db $CE,$ED,$66,$66,$CC,$0D,$00,$0B,$03,$73,$00,$83,$00,$0C
.db $00,$0D,$00,$08,$11,$1F,$88,$89,$00,$0E,$DC,$CC,$6E,$E6
.db $DD,$DD,$D9,$99,$BB,$BB,$67,$63,$6E,$0E,$EC,$CC,$DD,$DC
.db $99,$9F,$BB,$B9,$33,$3E

.org $0150
start:
di                            ;desactivation des interruption
ld  sp, $fff4                 ;Place le pointeur du stack � FFF4 (adresse recommand�e par nintendo)

waitvblank:                   ;attend que l'�cran ait fini de rafraichir (FF44 �tant un registre indiquant quelle ligne sut laquelle le controleur d'affichage Travail)
ldh a, ($44)
cp 144                        ;comparer regisre A avec 144
jr c, waitvblank              ;si a<144 retourner � waitvblank

xor a
ldh ($40), a                  ;registre configuration d'�cran = a = 0  (afin d'�teindre l'�cran)


;chargement des tiles dans la VRAM

;ld bc, 640                    ;nombre d'octets � transf�rer (une tile vaut 16 octets, 40 tiles dans le jeu donc 16*40=640) � resoudre plus tard:(Ne marche pas avec BC)

ld de, tiles                  ;mise de l'adresse du label tile (adresse des tiles dans la rom) dans DE
ld hl, $8000                  ;$8000 = adresse du tile 0 dans la VRAM

ld b, 254

loadtiles:
ld a, (de)
ldi (hl), a                   ;ins�rer donn�e dans l'adresse point�e par hl et incr�menter hl
inc de                        ;incr�menter de
dec bc                        ;d�cr�menter le compteur d'it�rations
ld a, c
or b                          ;les deux derni�re op�ration servent � m�langer b et c car le flag z ne se met pas � 1 si le resultat d'une op�ration 16 bit est 0 il faut donc verifier � l'aide d'un or que tut soit � 0 mais faire l'op�ration sur un registre 8 bit
jr nz, loadtiles              ;sauter � loadtiles si bc est diff�rent de 0

;fin de l'insertion des tiles dans la vram




;insertion du background

ld hl, $9800                  ;9800 est la premi�re adresse du premier tableau de background

;140 premi�re tiles grise claire (tile 16)
ld b, 20                      ;NB tiles premi�re ligne (de l'ecran affichable)
ld a, 16                      ;a = num�ro de Tile
ld e, 7                       ;NB tiles horizontales

background1:
ldi (hl), a
dec b
jr z, background1e2          ; si b = 0 sauter � background1 (lorsque la ligne est compl�t�e)
jr background1               ;sauter � background1

background1e2:
ld bc, 12         ;12 = NB de tiles en dehors de l'�cran sur la ligne
add hl, bc
dec e
ld b, 20                 ;remettre compteur d'it�ration� 20
jr nz, background1           ;si c est diff�rent de 0 sauter � background1 (si ilreste des ligne horizontales )

;15 tiles premi�re blanche du haut de la route (tile 21)
ld b, 15
ld a, 21

background2:
ldi (hl), a
dec b
jr nz, background2         ;si b diff�rent de 0 sauter � background2 (lorsque ligne pas finie)

;1�re tiles ligne d'arriv�e (tile 22)
inc a                       ;faire passer a de 21 � 22
ldi (hl), a

;4 derni�re tiles blanche premi�re ligne route (tile 21)
dec a                       ;faire passer a de 22 � 21
ldi (hl), a
ldi (hl), a
ldi (hl), a
ldi (hl), a

ld bc, 12         ;12 = NB de tiles en dehors de l'�cran sur la ligne
add hl, bc

;15 premi�re tiles deuxi�me ligne route (tiles 17-18)
ld b, 7                     ;dans les 15 premi�re tiles 7 paire de tiles + 1 autre tile ins�r�e en dehors de la boucle
ld a, 17

background3:
ldi (hl), a
inc a                       ;passer � 18
ldi (hl),a
dec a                       ;passer � 17
dec b
jr nz, background3          ;si b est diff�rent de 0 sauter � background3

ldi (hl), a                 ;dernier tile qui ne pouvait pas s'ins�rer dans la boucle car NB tiles impair

;tiles ligne d'arriv�e deuxi�me ligne route (tile 23)
ld a, 23
ldi (hl), a

;4 derni�re tiles de la deuxi�me ligne (tiles 21-18-17-18)
dec a
dec a                        ;passer de 23 � 21
ldi (hl), a
ld a, 18
ldi (hl), a
dec a                        ;passer de 18 � 17
ldi (hl), a
inc a                        ;passer de 17 � 18
ldi (hl), a

ld bc, 12         ;12 = NB de tiles en dehors de l'�cran sur la ligne
add hl, bc

;15 premi�re tiles troisi�me ligne route (tiles 19-20)
ld b, 7                     ;dans les 15 premi�re tiles 7 paire de tiles + 1 autre tile ins�r�e en dehors de la boucle
ld a, 19

background4:
ldi (hl), a
inc a                       ;passer � 20
ldi (hl),a
dec a                       ;passer � 19
dec b
jr nz, background4          ;si b est diff�rent de 0 sauter � background4

ldi (hl), a                 ;dernier tile qui ne pouvait pas s'ins�rer dans la boucle car NB tiles impair

;tiles ligne d'arriv�e deuxi�me ligne route (tile 24)
ld a, 24
ldi (hl), a

;4 derni�re tiles de la deuxi�me ligne (tiles 21-20-19-20)
dec a
dec a
dec a                        ;passer de 24 � 21
ldi (hl), a
dec a                        ;passer de 21 � 20
ldi (hl), a
dec a                        ;passer de 20 � 19
ldi (hl), a
inc a                        ;passer de 19 � 20
ldi (hl), a

ld bc, 12         ;12 = NB de tiles en dehors de l'�cran sur la ligne
add hl, bc

;15 tiles premi�re blanche du bas de la route (tile 21)
ld b, 15
ld a, 21

background5:
ldi (hl), a
dec b
jr nz, background5         ;si b diff�rent de 0 sauter � background2 (lorsque ligne pas finie)

;1�re tiles ligne d'arriv�e (tile 22)
inc a                       ;faire passer a de 21 � 22 (la tile du bas de la ligne d'arriv�e est la m�me que celle du haut)
ldi (hl), a

;4 derni�re tiles blanche derni�re ligne route (tile 21)
dec a                       ;faire passer a de 22 � 21
ldi (hl), a
ldi (hl), a
ldi (hl), a
ldi (hl), a

ld bc, 12         ;12 = NB de tiles en dehors de l'�cran sur la ligne
add hl, bc

;140 derni�re tiles grise claire (tile 16)
ld b, 20                      ;NB tiles premi�re ligne (de l'ecran affichable)
ld a, 16                      ;a = num�ro de Tile
ld e, 7                       ;NB tiles horizontales

background6:
ldi (hl), a
dec b
jr z, background6e2          ; si b = 0 sauter � background1 (lorsque la ligne est compl�t�e)
jr background6               ;sauter � background1

background6e2:
ld bc, 12         ;12 = NB de tiles en dehors de l'�cran sur la ligne
add hl, bc
dec e
ld b, 20                 ;remettre compteur d'it�ration� 20
jr nz, background6           ;si c est diff�rent de 0 sauter � background1 (si ilreste des ligne horizontales )

;mise � 0 des valeur de scroll du background
xor a
ldh ($42), a
ldh ($43), a

;fin de l'insertion du background




;mise en place des sprites dans l'OAM

;nettoyage de l'OAM
ld hl, $ff00                     ;ff00 = premi�re adresse de l'OAM
ld b, 160                        ; nombre d'octet dans l'OAM

clearoam:
ld (hl), 0                       ;on ne peux pa faire ldi (hl) dans les adresse de l'OAM � cause d'une erreur de conception
inc l
dec b
jr nz, clearoam

;placement des donn�e des sprites (les sprite sont en 8 par 16)

;voiture 1 et 2 (premier sprite voiture 1 X: 8 Y: 72 ; premier sprite voiture 2 X: 8 Y: 88)
ld hl, $fe00
ld a, 8                          ;a=x
ld b, 72                         ;b=y
ld c, 0                          ;c=num�ro Tiles
ld d, 4                          ;compteur d'it�rations sprites
ld e, 2                          ;compteur d'it�rations voitures

car1and2:
ld (hl), b                       ;set y
inc l
ld (hl), a                       ;set x
add 8                            ;x du sprite suivant
inc l
ld (hl), c                       ;set sprite tiles
inc c
inc c                            ;prochaine premi�re tile du sprite suivant
inc l
ld (hl), 0                       ;mettre tout les attribut du sprite � 0
inc l
dec d
jr z, car1to2
jr car1and2

car1to2:
ld b, 88                         ;b=y
ld a, 8                          ;a=x
ld d, 4
dec e
jr nz, car1and2

;fin du placement des sprites


;initialisation des variable
ld a, 8
ld (p1posx), a
ld a, 8
ld (p2posx), a
ld a, 0
ld (flag), a
ld (flag2), a



;initialisation des palettes
ld a, %11100100                  ;11=Noir 10=Gris fonc� 01=Gris clair 00=Blanc/transparent
ldh ($47), a                     ;palette background
ldh ($48), a                     ;palette sprite 1
ld a, %10110100                  ;10=Noir 11=Gris fonc� 01=Gris clair 00=Blanc/transparent
ldh ($49), a                     ;palette sprite 2


;reactivation et configurration affichage
ld a, %10010111                 ;Ecran on, Background on, windows off, tiles � $8000, sprite 8 par 16, background priorit� sur windows
ldh ($40), a

;activer interruption (Vblank)
ld a, %00010000                 ;Utilisations des interruption vblank
ldh ($41), a
ld a, %00000001                 ;activations des interruption Vblank
ldh ($FF), a
ei ;Activer les interruption




;faire lire � la game boy les bouton
ld a, %00010000
ldh ($00), a

waitstart:
ld a, ($ff00)
bit 3, a
jr nz, waitstart                ;tant que pas press� sauter � waitstart




;boucle pricipale


;voiture 1
car1:
ld a, %00100000                  ;lecture de la croix directionelle
ldh ($00), a
ld a, ($ff00)                    ;mettre octet des entr�e joypad dans a
ld a, ($ff00)
ld a, ($ff00)
ld a, ($ff00)
ld a, ($ff00)                    ;on le fait plusieur fois pour limiter les parasites du au vibration m�quanique du bouton (sinon les voiture peuvent avancer toute seule)
bit 1, a                         ;tester bit 1 (fl�che gauche)
jr z, p1go                       ;si bit 1 = 0 (donc appuy�) alors aller � p1go
ld a, (flag)                     ;si on arrive la �a veut dire que le bouton n'est pas appuy� donc on met le bit 0 de flag � 0
res 0, a
ld (flag), a
jr car2

p1go:
ld a, (flag)                     ;test du bit 0 de flag
bit 0, a
jr nz, car2                      ;si le bit 0 de flag n'est pas � 0 alors sauter � car2 (car �a veut dire que le bouton n'a pas �t� lach�)
ld a, (p1posx)                   ;mettre position x player 1 dans a
inc a                            ;incr�menter la position de 1 px
ld (p1posx), a                   ;remettre nouvelle position dans variable
ld a, (flag)                     ;on met le bit 0 de flag � 1 pour indiqu� qu'il a �t� appuy�
set 0, a
ld (flag), a
jr car2

;voiture 2
car2:
ld a, %00010000                  ;lecture des bouton
ldh ($00), a
ld a, ($ff00)                    ;mettre octet des entr�e joypad dans a
ld a, ($ff00)
ld a, ($ff00)
ld a, ($ff00)
ld a, ($ff00)                    ;on le fait plusieur fois pour limiter les parasites du au vibration m�quanique du bouton (sinon les voiture peuvent avancer toute seule)
bit 0, a                         ;tester bit 0 (bouton a)
jr z, p2go                       ;si bit 0 = 0 (donc appuy�) alors aller � p2go
ld a, (flag)                     ;si on arrive la �a veut dire que le bouton n'est pas appuy� donc on met le bit 1 de flag � 0
res 1, a
ld (flag), a
jr winner

p2go:
ld a, (flag)                     ;test du bit 1 de flag
bit 1, a
jr nz, winner                      ;si le bit 1 de flag n'est pas � 0 alors sauter � winner? (car �a veut dire que le bouton n'a pas �t� lach�)
ld a, (p2posx)                   ;mettre position x player 2 dans a
inc a                            ;incr�menter la position de 1 px
ld (p2posx), a                   ;remettre nouvelle position dans variable
ld a, (flag)                     ;on met le bit 1 de flag � 1 pour indiqu� qu'il a �t� appuy�
set 1, a
ld (flag), a


;savoir s'il y  a un gagnant
winner:
ld a, (p1posx)
cp 136
jr c, winner2
ld a, (flag2)
set 0, a
ld (flag2), a

ld a, (p2posx)
cp 136
jp c, car1                        ; je n'arrive pas � mettre un jump relative si il y en a d�ja un avant (pour plus tard: trouver pourquoi)
ld a, (flag2)
set 1, a
ld (flag2), a
jp car1

winner2:
ld a, (p2posx)
cp 136
jp c,car1
ld a, (flag2)
set 1, a
ld (flag2), a
jp car1



;routine Vblank
vblank:
;empiler les registre
push af
push hl
push bc
push de

;changer position des sprite dans l'oam
ld a, (p1posx)
ld hl, $fe01
ld bc, 4                        ;nombre d'adresse � sauter pour passer � x du sprite suivant
ld d, 4                         ;compteur de sprite de voiture
ld e, 2                         ;compteur de voitures

pos1and2:
ld (hl), a
add 8
add hl, bc
dec d
jr z, pos1to2
jr pos1and2

pos1to2:
ld a, (p2posx)
ld d, 4
dec e
jr nz, pos1and2


;afficher texte gagnant
ld a, (flag2)
bit 0, a
jr z, p2win

bit 1, a
jr nz, bothwin


;ins�rer texte p1
ld a, 25
ld ($9845), a
ld a, 26
ld ($9865), a
ld a, 27
ld ($9846), a
ld a, 28
ld ($9866), a
jp win


;ins�rer texte both
bothwin:
ld a, 31
ld ($9844), a
ld ($9864), a
ld a, 32
ld ($9845), a
ld a, 40
ld ($9865), a
ld a, 33
ld ($9846), a
ld a, 34
ld ($9866), a
ld a, 35
ld ($9847), a
ld a, 41
ld ($9867), a
jp win



;ins�rer texte p2
txtp2:
ld a, 25
ld ($9845), a
ld a, 26
ld ($9865), a
ld a, 29
ld ($9846), a
ld a, 30
ld ($9866), a

;ins�rer texte win
win:
ld a, 36
ld ($9848), a
ld a, 37
ld ($9868), a
ld a, 42
ld ($9849), a
ld a, 43
ld ($9869), a
ld a, 34
ld ($984a), a
ld ($986a), a
ld a, 38
ld ($984b), a
ld a, 39
ld ($986b), a




p2win:
ld a, (flag2)                                ;Pour plus tard: comprendre pourquoi si je ne met pas ld a, (flag2) le programme affiche que p2 � gagn� aors que ce n'est pas le cas
bit 1, a
jp nz, txtp2


;d�piler registres
pop de
pop bc
pop hl
pop af
ret

.ORG   $0800
tiles:
.INCBIN "CarsgameGB_Tiles.bin"
